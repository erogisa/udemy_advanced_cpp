#include <iostream>
#include <vector>
#include <map>

class Test{
private:
	int _id = 0;
	std::string _name = "";
	std::vector<int> *_data = NULL;

public:
	Test(): _id(0), _name(""){
		_data = new std::vector<int>();
	}

	Test(int id, std::string name): _id(id), _name(name){
		if(!_data){
			_data = new std::vector<int>();
		}
		for(int i = 0; i < 10; ++i){
			_data->push_back(i+_id);
		}
	}

	Test(const Test &copy){
		*this = copy;
		std::cout << "Copy running!" << std::endl;
	}

	~Test(){
		delete _data;
		std::cout << "Deleted Data!" << std::endl <<std::flush;
	}

	bool operator<(const Test &compare) const{
		return _id < compare._id;
	}

	friend std::ostream &operator<<(std::ostream &os, const Test &in){
		os << in._id << " " << in._name;
		for(const auto i : *in._data){
			os << i;
		}
		os << std::flush;
		return os;
	}

	static bool compare(const Test *a, const Test *b){
		return a->_id < b->_id;
	}

	const Test &operator=(const Test &a){
		_id = a._id;
		_name = a._name;

		if(!_data){
			_data = new std::vector<int>();
		}

		// Deep copy
		*_data = *a._data;
		std::cout << "Assignment running!" << std::endl;
		return *this;
	}

};


int main(int argc, char **argv) {

	Test test1 = Test(10, "Kalle");
	Test test2 = Test(30, "Pelle");
	//Copy initialization
	Test test3 = test2;

	std::cout << test1 << std::endl;
	std::cout << test2 << std::endl;

	test1 = test2;

	std::cout << test1 << std::endl;
	std::cout << test2 << std::endl;

	std::cout << "Hello world" << std::endl;
	return 0;
}
