#include <iostream>
#include <typeinfo>

template<typename T, typename S>
auto test(T value, S another) -> decltype(value + another){
	return value + another;
}

int get(){
	return 999;
}

auto test2() -> decltype(get()){
	return get();
}

int main(int argc, char **argv) {

	auto value = 7;

	auto strings = std::string("Nisse");

	//std::cout << typeid(strings).name() << std::endl;

	std::cout << test(std::string("Kldfdfdfdäff"), std::string("dfds")) << std::endl;

	std::cout << test2() << std::endl;

	std::cout << "Hello world";
	return 0;
}
