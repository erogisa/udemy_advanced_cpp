#include <iostream>
#include <typeinfo>
#include <vector>

int main(int argc, char **argv) {

	auto texts = {"Kalle", "Nisse", "Olle", "Pelle"};

	for(auto &i : texts){
		std::cout << i << std::endl;
	}

	std::vector<int> vec = std::vector<int>();

	vec.push_back(10);
	vec.push_back(20);
	vec.push_back(30);

	for(auto elem : vec){
		std::cout << elem << std::endl;
	}

	std::string hello = "Hello Allan";

	for(auto c : hello){
		std::cout << c << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}

