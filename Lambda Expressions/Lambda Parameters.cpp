#include <iostream>
#include <vector>

void testGreet(void (*fptr)(std::string) ){
	fptr("Bob");
}

void runFunc(double (*fptr)(double a, double b)){
	auto val = fptr(10.0, 2.2);
	std::cout << val << std::endl;
}

double divFunc(double a, double b){
	if(b == 0){
		return 0;
	}
	return a/b;
}

int main(int argc, char **argv) {

	auto pGreet = [](std::string name){ std::cout << "Hello " << name << std::endl;};

	pGreet("Nisse");

	testGreet(pGreet);

	auto pDivide = [](double a, double b) -> double{
		if(b == 0){
			return 0;
		}
		return a / b;
	};

	auto pMult = [](double a, double b) -> double{return a * b;};

	runFunc(pDivide);
	runFunc(pMult);
	runFunc(divFunc);

	auto div = pDivide(10.0, 3.3);
	auto mult = pMult(10.0, 3.3);

	std::cout << div << " : " << mult << std::endl;


	std::cout << "Hello world";
	return 0;
}
