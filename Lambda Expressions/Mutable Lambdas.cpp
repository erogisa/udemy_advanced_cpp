#include <iostream>
#include <vector>


int main(int argc, char **argv) {

	int cats = 5;

	auto lambda = [cats]()mutable -> bool{
		cats += 10;
		std::cout << cats << std::endl;
		return true;};
	std::cout << lambda() << std::endl;
	std::cout << cats << std::endl;

	std::cout << "Hello world";
	return 0;
}
