#include <iostream>
#include <vector>

class Test{
	int one = 1;
	int two = 2;

public:
	void run(){
		int three = 3;
		int four = 4;
		auto pLambda = [&](){
			two = 666;
			std::cout << "Hello Lambda: " << three << std::endl;
			std::cout << "Hello instance: " << two << std::endl;
		};
		pLambda();
	}
};


int main(int argc, char **argv) {
	Test test;
	test.run();


	std::cout << "Hello world";
	return 0;
}
