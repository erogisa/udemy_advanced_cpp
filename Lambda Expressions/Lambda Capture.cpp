#include <iostream>
#include <vector>

void test_lambda(void (*pfunc)()){
	pfunc();
}

int main(int argc, char **argv) {

	int one = 1;
	int two = 2;
	int three = 3;

	//Capture one, two and three by value
	[one, two, three](){std::cout << "Hello Lambda:" << one << " "<< two << " " << three << std::endl;}();

	//Capture all local variables, but three by reference
	[=, &three](){three = 9; std::cout << "Hello Lambda:" << one << " "<< two << " " << three << std::endl;}();

	//Capture all local variables by reference
	[&](){one = 99; std::cout << "Hello Lambda:" << one << " "<< two << " " << three << std::endl;}();


	std::cout << "Hello world";
	return 0;
}
