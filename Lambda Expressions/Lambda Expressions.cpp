#include <iostream>
#include <vector>

void test_lambda(void (*pfunc)()){
	pfunc();
}

int main(int argc, char **argv) {

	[](std::string s){ std::cout << s << std::endl; }("Test");

	auto test = [](){ std::cout << "hello" << std::endl; };

	test_lambda(test);

	test_lambda([](){ std::cout << "Hello Direct" << std::endl; });

	std::cout << "Hello world";
	return 0;
}
