/*
 * Complex.cpp
 *
 *  Created on: 23 maj 2020
 *      Author: rogerisaksson
 */
#include <iostream>
#include "Complex.h"

namespace udemy {

	Complex::Complex() :_real(0), _imaginary(0) {}

	Complex::Complex(double real, double imaginary) : _real(real), _imaginary(imaginary){}

	Complex::Complex(const Complex &copy){
		*this = copy;
	}

	Complex::~Complex(){

	}

	bool Complex::operator<(const Complex &rhs){
		return (_real == rhs._real) && (_imaginary == rhs._imaginary);
	}

	bool Complex::operator==(const Complex &rhs) const{
		return _real == rhs._real && _imaginary == rhs._imaginary;
	}

	bool Complex::operator!=(const Complex &rhs) const{
		return !(*this==rhs);
	}

	bool Complex::operator==(double rhs) const{
		return _real == rhs;
	}

	bool Complex::operator!=(double rhs) const{
		return !(*this==rhs);
	}

	const Complex &Complex::operator=(const Complex &rhs){
		_real = rhs._real;
		_imaginary = rhs._imaginary;
		return *this;
	}

	std::ostream & operator<<(std::ostream &lhs, const Complex &rhs){
		lhs << rhs._real << ' ' << rhs._imaginary << 'i' << std::flush;
		return lhs;
	}

	const Complex Complex::operator+(const Complex &rhs) const{
		return Complex(_real + rhs._real, _imaginary + rhs._imaginary);
	}

	Complex operator+(const Complex &lhs, const double rhs){
		return Complex(lhs._real + rhs, lhs._imaginary);
	}

	Complex operator+(const double lhs, const Complex &rhs){
		return rhs+lhs;
	}

	Complex &Complex::operator+=(const Complex &rhs){
		_imaginary += rhs._imaginary;
		*this += rhs._real;
		return *this;
	}

	Complex &Complex::operator+=(const double rhs){
		_real += rhs;
		return *this;
	}

	// Complex Conjugate
	Complex &Complex::operator*(){
		_imaginary = -_imaginary;
		return *this;
	}

	const Complex Complex::operator*(const Complex &rhs) const{
		return Complex(_real * rhs._real +
						(_imaginary * rhs._imaginary) * -1.0,
						_real * rhs._imaginary +
						_imaginary * rhs._real);
	}

	Complex operator*(const double lhs, const Complex &rhs){
		return Complex(rhs._real*lhs, rhs._imaginary*lhs);
	}

	Complex operator*(const Complex &lhs, const double rhs){
		return rhs*lhs;
	}

	Complex &Complex::operator*=(const Complex &rhs){
		*this = (*this)*rhs; // Call complex multiplication
		return *this;
	}

	Complex &Complex::operator*=(const double rhs){
		_real *= rhs;
		_imaginary *= rhs;
		return *this;
	}

	// Define prefix increment operator.
	Complex &Complex::operator++(){
		++_real;
		return *this; // Return directly after incrementing it
	}

	// Define postfix increment operator.
	Complex Complex::operator++(int){
		Complex temp = *this;
		++(*this); // Call prefix increment operator
		return temp; // Return value before incrementing it
	}

	// Define prefix decrement operator.
	Complex &Complex::operator--(){
		--_real;
		return *this; // Return directly after incrementing it
	}

	// Define postfix decrement operator.
	Complex Complex::operator--(int){
		Complex temp = *this;
		--(*this); // Call prefix increment operator
		return temp; // Return value before incrementing it
	}

} /* namespace udemy */
