/*
 * complex.h
 *
 *  Created on: 23 maj 2020
 *      Author: rogerisaksson
 */

#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <iostream>

namespace udemy {

	class Complex {
	private:
		double _real;
		double _imaginary;

	public:
		Complex();
		Complex(double real, double imaginary);
		Complex(const Complex &lhs);

		~Complex();

		const Complex &operator=(const Complex &lhs);

		bool operator==(const Complex &rhs) const;
		bool operator==(double rhs) const;

		bool operator!=(const Complex &rhs) const;
		bool operator!=(double rhs) const;

		bool operator<(const Complex &rhs);

		friend std::ostream &operator<<(std::ostream &lhs, const Complex &rhs);

		Complex &operator+=(const Complex &rhs);
		Complex &operator+=(const double rhs);

		const Complex operator+(const Complex &rhs) const;
		friend Complex operator+(const double lhs, const Complex &rhs);
		friend Complex operator+(const Complex &lhs, const double rhs);

		Complex &operator*=(const Complex &rhs);
		Complex &operator*=(const double rhs);

		const Complex operator*(const Complex &rhs) const;
		friend Complex operator*(const double lhs, const Complex &rhs);
		friend Complex operator*(const Complex &rhs, const double lhs);

		// Complex conjugate
		Complex &operator*();

		// Define prefix increment operator.
		Complex &operator++();

		// Define postfix increment operator.
		Complex operator++(int);

		// Define prefix decrement operator.
		Complex &operator--();

		// Define postfix decrement operator.
		Complex operator--(int);


	};

} /* namespace udemy */

#endif /* COMPLEX_H_ */
