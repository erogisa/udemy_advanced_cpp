
#include "Complex.h"

int main(int argc, char **argv) {

	udemy::Complex complex1 = udemy::Complex(10, 20);
	udemy::Complex complex2 = udemy::Complex(0, 10);
	udemy::Complex *complex3 = new udemy::Complex(30, 100);

	*complex3 = complex1 + complex2 + *complex3;

	*complex3 = *complex3 + 100.0;

	*complex3 += complex2;

	++(*complex3);

	std::cout << 3.2 + *complex3 +1 + complex1 + 100 + complex2 << std::endl;
	std::cout << "Hello world" << std::endl;
	return 0;
}
