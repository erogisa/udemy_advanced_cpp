#include <iostream>
#include <stack>
#include <queue>

class Test{
private:
	int _id;
	std::string _name;

public:
	Test() : _id(0), _name(""){}

	Test(int id, std::string name) : _id(id), _name(name){}

	Test(const Test &copy) : _id(copy._id), _name(copy._name){}

	~Test(){
		_id = 0;
		_name = "";
		std::cout << "Destructor hit" << std::endl;
	}

	bool operator<(const Test &compare) const{
		return _name < compare._name;
	}

	friend std::ostream &operator<<(std::ostream &os, const Test &test){
		os << test._id << " " << test._name << std::flush;
		return os;
	}
};

int main(int argc, char **argv) {

	std::stack<Test*> test_stack;

	test_stack.push(new Test(10, "Nisse"));
	test_stack.push(new Test(10, "Olle"));
	test_stack.push(new Test(10, "Pelle"));
	test_stack.push(new Test(10, "Kalle"));

	while(!test_stack.empty()){
		std::cout << *test_stack.top() << std::endl;
		delete test_stack.top();
		test_stack.pop();
	}

	std::cout << "Queues" <<std::endl;

	std::queue<Test*> test_queue;

	test_queue.push(new Test(10, "Nisse"));
	test_queue.push(new Test(10, "Olle"));
	test_queue.push(new Test(10, "Pelle"));
	test_queue.push(new Test(10, "Kalle"));

	while(!test_queue.empty()){
		std::cout << *test_queue.front() << std::endl;
		delete test_queue.front();
		test_queue.pop();
	}

	std::cout << "Hello world";
	return 0;
}

