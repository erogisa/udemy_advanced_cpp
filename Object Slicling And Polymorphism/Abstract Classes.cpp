#include <iostream>

class Animal{

public:
	virtual void speak() = 0;
	virtual void run() = 0;
	virtual ~Animal(){std::cout << "Animal Deleted" << std::endl;}

protected:
    Animal(){}

};

class Dog : public Animal{
public:

	virtual void speak(){
		std::cout << "Howl!" << std::endl;
	}
	//Dog(){}

   ~Dog(){std::cout << "Dog Deleted" << std::endl;}

};

class Labrador : public Dog{
public:

	virtual void run(){
		std::cout << "Running!" << std::endl;
	}
	Labrador(){}
	virtual ~Labrador(){std::cout << "Labrador Deleted" << std::endl;}

};

void test(Animal &aml){
	aml.speak();
}

int main(int argc, char **argv) {

	Labrador dog;
	dog.speak();
	dog.run();
	test(dog);

	Animal *animals[5];

	animals[0] = new Labrador();

	std::cout << "Hello world" << std::endl;
	return 0;
}
