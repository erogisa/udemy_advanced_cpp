#include <iostream>
#include <vector>

class Test{

};

void check(Test &copy){
	std::cout << "L-value" << std::endl;

}

void check(Test &&rref){
	std::cout << "R-value" << std::endl;
}

template<typename T>
void call(T &&rval){
	// forward maintains the R or L value
	check(std::forward<T>(rval));
}

int main(int argc, char **argv) {

	Test test;
	auto &&t = Test();
	call(test); 	//L-value since test is a reference
	call(Test()); 	//R-value since the constructor returns a temporary


	std::cout << "Hello world";
	return 0;
}
