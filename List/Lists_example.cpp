#include <iostream>
#include <list>
#include <boost/thread/thread.hpp>

int main(int argc, char **argv) {

	std::list<int> numbers;

	boost::thread();

	for(int i = 0; i < 10; ++i)
	{
		numbers.push_back(i);
		numbers.push_front(i*10);
	}

	std::list<int>::iterator it = numbers.begin();

	it++;

	numbers.insert(it, 100);

	it = numbers.erase(--it);

	for(auto &elem : numbers){
		std::cout << elem << std::endl;
	}

	boost::thread thr = boost::thread();

	std::cout << "List element is: " << *(it) << std::endl;

	return 0;
}
