#include <iostream>
#include <vector>

class Test{
	int _id{3};
	std::string _name{"Mike"};
public:
	Test() = default;

	Test(const Test &copy) = default;

	Test &operator=(const Test &rhs) = default;

	Test(int id) : _id(id){

	}
	void print(){
		std::cout << _id << " " << _name << std::endl;
	}
};

int main(int argc, char **argv) {

	Test a;
	a.print();

	Test b{50};
	b.print();

	a = b;

	Test c = a;

	std::cout << "Hello world";
	return 0;
}
