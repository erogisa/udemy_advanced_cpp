#include <iostream>
#include <vector>
#include <functional>

using namespace std;
using namespace placeholders;
class Test{
public:

	int add(int a, int b, int c){
		std::cout << a << ", " << b << ", " << c << std::endl;
		return a+b+c;
	}
};

int add(int a, int b, int c){
		std::cout << a << ", " << b << ", " << c << std::endl;
		return a+b+c;
	}

int run(std::function<int(int, int)> func){
	return func(7,3);
}

int main(int argc, char **argv) {

	auto fadd = std::bind(add, _2, 100, _1);
	std::cout << fadd(1, 2) << std::endl;
	std::cout << run(fadd) << std::endl;

	Test test;
	auto objbind = std::bind(&Test::add, test, _2, 100, _1);

	std::cout << "Hello world";
	return 0;
}
