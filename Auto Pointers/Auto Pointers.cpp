#include <iostream>
#include <memory>

class Test {
private:
	int val = 0;
public:
	static int test;
	Test() {
		std::cout << "Created" << std::endl << std::flush;
		val = test;
		test++;
	}

	~Test() {
		std::cout << "Deleted" << std::endl;
	}

	void greet() {
		std::cout << "Test Hello #: " << val << std::endl << std::flush;
	}
};
int Test::test = 0;

class Temp {
private:
	int val = 0;
public:
	static int temp;
	std::unique_ptr<Test[]> testptr;
	Temp() :
			testptr(new Test[2]) {
		val = temp;
		temp++;

	}

	void greet() {
		std::cout << "Temp Greet #: " << val << std::endl << std::flush;
	}
};
int Temp::temp = 0;

int main(int argc, char **argv) {
	std::cout << "Unique Pointers" << std::endl << std::endl;
	{
		std::unique_ptr<int> intptr(new int);
		std::unique_ptr<Test[]> testptr(new Test[5]);
		testptr[1].greet();
		Temp temp;
	}

	std::cout << std::endl;
	std::cout << "Shared Pointers" << std::endl << std::endl;

	{
		{
			Temp::temp = 0;
			Test::test = 0;
			std::shared_ptr<Test> pTest2(nullptr);

			{
				auto pTest1 = std::make_shared<Test>();
				pTest2 = pTest1;
			}

			pTest2.get()->greet();

			Temp::temp = 0;
			Test::test = 0;
		}

		std::cout << "Shared Pointers Array" << std::endl << std::endl;

		std::shared_ptr<Temp> tempptr(new Temp[2], [](Temp *p) {
			delete[] p;
		});

		for (auto ptr = tempptr.get(); ptr < tempptr.get() + 2; ptr++) {
			ptr->greet();
			for (auto pptr = ptr->testptr.get(); pptr < ptr->testptr.get() + 2;
					pptr++) {
				pptr->greet();
			}
		}
	}

	std::cout << "Hello world" << std::endl;
	return 0;
}
