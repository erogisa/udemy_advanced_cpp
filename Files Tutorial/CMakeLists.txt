cmake_minimum_required (VERSION 2.6)

SET(CMAKE_CXX_STANDARD 17)
SET (CMAKE_BUILD_TYPE Debug)
SET(CMAKE_CXX_FLAGS_DEBUG "-O0 -g3")
SET(CMAKE_C_FLAGS_DEBUG "-O0 -g3")
SET(Boost_USE_DEBUG_LIBS ON)
SET(BOOST_INCLUDE_DIRS /usr/local/Cellar/boost/1.72.0_2/include)

project (projectname)

set(BOOST_INCLUDE_DIRS /usr/local/Cellar/boost/1.72.0_2/include)

add_executable("Writing Text Files" "Writing Text Files.cpp")
add_executable("Reading Text Files" "Reading Text Files.cpp")
add_executable("Parsing Text Files" "Parsing Text Files.cpp")
add_executable("Packing Structs" "Packing Structs.cpp")
add_executable("Binary Files" "Binary Files.cpp")

target_include_directories("Writing Text Files" PUBLIC ${BOOST_INCLUDE_DIRS})
