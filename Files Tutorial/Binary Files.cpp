#include <iostream>
#include <fstream>

//byte align the struct
#pragma pack(push, 1)
struct Person{
	char name[50];
	int age;
	double weight;
};
#pragma pack(pop)


int main(int argc, char **argv) {

	std::string file_name = "binary_file.bin";

	std::ofstream output_stream;

	output_stream.open(file_name, std::ios::binary);

	if(!output_stream.is_open()){
		return 1;
	}

	Person person;

	std::strcpy(person.name,"Nisse");
	person.age = 25;
	person.weight = 60.12;

	output_stream.write(reinterpret_cast<char *>(&person), sizeof(Person));

	output_stream.close();

	std::ifstream input_stream;

	input_stream.open(file_name, std::ios::binary);

	if(!input_stream.is_open()){
		return 1;
	}

	Person someone_else = {};

	input_stream.read(reinterpret_cast<char *>(&someone_else), sizeof(Person));

	std::cout << someone_else.name << ' ' << someone_else.age << ' ' << someone_else.weight << std::endl;
	return 0;
}

