#include <iostream>
#include <fstream>

//byte align the struct
#pragma pack(push, 1)
struct Person{
	char name[50];
	int age;
	double weight;
};
#pragma pack(pop)


int main(int argc, char **argv) {

	std::cout << sizeof(Person) << std::endl;
	std::cout << "Hello world";
	return 0;
}

