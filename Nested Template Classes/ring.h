/*
 * ring.h
 *
 *  Created on: 24 maj 2020
 *      Author: rogerisaksson
 */

#ifndef RING_H_
#define RING_H_

#include <iostream>
#include <algorithm>

namespace udemy {
	template<class T>
	class ring {
	private:
		T *_values = nullptr;
		unsigned int _size = 3;
		unsigned int _pos = 0;

	public:
		class iterator;

		ring(int max_items) : _values(NULL), _size(max_items), _pos(0){
			_values = new T[_size];
		}

		ring(const ring &copy){
			*this = copy;
			_values = nullptr;
			_values = new T[_size];
			std::memcpy(_values, copy._values, sizeof(T)*_size);
		}

		~ring(){
			delete[] _values;
		}

		iterator begin(){
			return iterator(0, *this);
		}

		iterator end(){
			return iterator(_size, *this);
		}

		int size(){
			return _size;
		}

		void add(T item){
			_values[_pos++] = item;
			if(_pos == _size){
				_pos = 0;
			}
		}

		T &get(int i){
			return _values[i];
		}
	};

	template<class T>
	class ring<T>::iterator{

	private:
		int _itpos;
		ring _ring;

	public:
		iterator(int pos, const ring &a_ring) : _itpos(pos), _ring(a_ring){}

		//postfix operator
		iterator operator++(int){
			iterator it = *this;
			++_itpos;
			return it;
		}

		//prefix operator
		iterator &operator++(){
			++_itpos;
			return *this;
		}

		T &operator*(){
			return _ring.get(_itpos);
		}

		bool operator!=(const iterator &rhs) const{
			return _itpos != rhs._itpos;
		}
	};

} /* namespace udemy */

#endif /* RING_H_ */
