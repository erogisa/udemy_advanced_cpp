#include <iostream>
#include <typeinfo>
#include <vector>
#include "ring.h"



int main(int argc, char **argv) {

	udemy::ring<std::string> textring(4);

	textring.add("one");
	textring.add("two");
	textring.add("three");
	textring.add("four");
	textring.add("five");
	textring.add("six");

	for(auto &item : textring){
		std::cout << item << std::endl;
	}

	std::cout << std::endl;

	for(udemy::ring<std::string>::iterator it = textring.begin(); it != textring.end(); it++){
		std::cout << *it << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}

