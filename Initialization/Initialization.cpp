#include <iostream>
#include <vector>

class C{
	public:

	std::string text;
	int id;
};

struct S{

	std::string text;
	int id;
};

//anonymous struct
struct{

	std::string text;
	int id;
}r1 = {"Hello", 7}, r2 = {" Pelle", 7};

int main(int argc, char **argv) {

	//c++ 98
	int values[] = {10, 20, 30, 40};

	//named
	C c1 = {"Nisse", 10};
	S d1 = {"Nisse", 10};

	//anonymous
	std::cout << r1.text << r2.text << std::endl;

	std::vector<std::string> strings;
	strings.push_back("Hello World");
	strings.push_back("Hello World");
	strings.push_back("Hello World");

	for(auto &string : strings){
			std::cout << string << std::endl;
	}

	int test{5};
	std::cout << test << std::endl;
	std::string text{"test"};
	std::cout << text << std::endl;

	int ints[]{10, 1, 2};
	std::cout << ints[1] << std::endl;

	int test2{};
	std::cout << test2 << std::endl;

	int *intptr{nullptr};
	std::cout << intptr << std::endl;

	//c++11 initialization
	std::vector<std::string> celevenstrings{"Kalle", "Pelle", "Olle"};

	for(auto &string : celevenstrings){
		std::cout << string << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}
