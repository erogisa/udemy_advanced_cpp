#include <iostream>
#include <vector>
#include <initializer_list>

using namespace std;

class Test{
public:
	Test(initializer_list<string> texts){
		for(auto text : texts){

		}
	}

	void print(initializer_list<string> texts){
		for(auto text : texts){
			cout << text << endl;
		}
	}
};

int main(int argc, char **argv) {

	Test testing{"Apple", "Orange", "Banana"};

	testing.print({"one", "two", "three"});

	std::cout << "Hello world";
	return 0;
}
