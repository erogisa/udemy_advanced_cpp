/*
 * fractalcreator.cpp
 *
 *  Created on: 28 maj 2020
 *      Author: rogerisaksson
 */

#include "fractal_creator.h"

namespace udemy {

fractal_creator::fractal_creator(int width, int height):
		_width(width),
		_height(height),
		_histogram(new int[udemy::mandelbrot::MAX_ITERATIONS]{}),
		_fractal(new int[_width* _height]{ 0 }),
		_bitmap(_width, _height),
		_zoomlist(_width, _height)
{
	_zoomlist.add(udemy::zoom(_width / 2, _height / 2, 4.0 / _width));

}

void fractal_creator::add_range(double end_range, const udemy::rgb_generator &rgb){
	_ranges.push_back(end_range*udemy::mandelbrot::MAX_ITERATIONS);
	_colors.push_back(rgb);
	if(_first_in_range){
		_ranges_total.push_back(end_range);
	}
	_first_in_range = true;
}

void fractal_creator::add_zoom(const udemy::zoom &zoom){
	_zoomlist.add(zoom);

}

void fractal_creator::generate_fractal_image(std::string filename){

	calculate_iterations();
	calculate_total_iterations();
	calculate_range_total();
	draw_fractal();
	write_bitmap(filename);
}

void fractal_creator::calculate_iterations(){
	for(int y = 0; y < _height; y++){
		for(int x = 0; x < _width; x++){

			std::pair<double, double> coords = _zoomlist.doZoom(x,y);
			int iterations = udemy::mandelbrot::getIterations(coords.first, coords.second);

			_fractal[y*_width + x]=iterations;

			if(iterations != udemy::mandelbrot::MAX_ITERATIONS) {

				_histogram[iterations]++;

			}
		}
	}
}

void fractal_creator::calculate_range_total(){

	int range_index = 0;

	for(int i = 0; i<udemy::mandelbrot::MAX_ITERATIONS; i++){
		int pixels = _histogram[i];
		_ranges_total[range_index] += pixels;
	}

	for(auto total : _ranges_total){
		std::cout << total << std::endl;
	}

}


void fractal_creator::calculate_total_iterations(){
	for(int i = 0; i<udemy::mandelbrot::MAX_ITERATIONS; ++i){
		_total_iterations+=_histogram[i];
	}
}

void fractal_creator::draw_fractal(){
	udemy::rgb_generator start_color(0, 0, 0);
	udemy::rgb_generator end_color(0, 0, 255);
	udemy::rgb_generator color_diff = end_color - start_color;

	for(int y = 0; y < _height; y++){
		for(int x = 0; x < _width; x++){

			std::uint8_t red = 0;
			std::uint8_t green = 0;
			std::uint8_t blue = 0;

			int iterations = _fractal[y*_width + x];

			if(iterations!=udemy::mandelbrot::MAX_ITERATIONS){

				double hue = 0.0;
				for(int i = 0; i <= iterations; i++){
					hue += ((double)_histogram[i])/(double)_total_iterations;
				}
				red = start_color._r + color_diff._r*hue;
				green = start_color._g + color_diff._g*hue;
				blue = start_color._b + color_diff._b*hue;
			}
			_bitmap.set_pixel(x, y, red, green, blue);
		}
	}

}

void fractal_creator::write_bitmap(std::string filename){
	_bitmap.write_file(filename);

}

} /* namespace udemy */
