/*
 * complex.h
 *
 *  Created on: 23 maj 2020
 *      Author: rogerisaksson
 */

#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <iostream>

namespace udemy {

	class Complex {
	private:
		double _real;
		double _imaginary;

	public:
		Complex();
		Complex(double real, double imaginary);
		Complex(const Complex &copy);

		~Complex();

		const Complex &operator=(const Complex &in);

		bool operator==(const Complex &a) const;
		bool operator==(double a) const;

		bool operator!=(const Complex &a) const;
		bool operator!=(double a) const;

		bool operator<(const Complex &compare);

		friend std::ostream &operator<<(std::ostream &os, const Complex &in);

		friend Complex operator+=(const Complex &a, const Complex &b);
		friend Complex operator+=(const Complex &a, double real);

		Complex operator+(const Complex &rhs) const;
		friend Complex operator+(double lhs, const Complex &rhs);
		friend Complex operator+(const Complex &lhs, double rhs);

		Complex operator*(const Complex &rhs) const;
		friend Complex operator*(double lhs, const Complex &rhs);
		friend Complex operator*(const Complex &rhs, double lhs);

		// Complex conjugate
		Complex operator*() const;

		// Define prefix increment operator.
		Complex &operator++();

		// Define postfix increment operator.
		Complex operator++(int);


	};

} /* namespace udemy */

#endif /* COMPLEX_H_ */
