/*
 * ring.h
 *
 *  Created on: 24 maj 2020
 *      Author: rogerisaksson
 */

#ifndef RING_H_
#define RING_H_

#include <iostream>
#include <deque>

namespace udemy {
	template<class T>
	class ring {
	private:
		T *_values;
		unsigned int _size;
		unsigned int _pos;

	public:
		class iterator;

		ring() :  _values(NULL), _size(3), _pos(0){
			_values = new T[_size];
		}

		ring(int max_items) : _values(NULL), _size(max_items), _pos(0){
			_values = new T();
		}

		//Copy constructor
		ring(const ring & rhs){
			*this = rhs;
		}

		~ring(){
			delete[] _values;
		}

		iterator begin(){
			return iterator(0, *this);
		}

		iterator end(){
			return iterator(_size, *this);
		}

		int size(){
			return _size;
		}

		void add(T item){
			_values[_pos++] = item;
			if(_pos == _size){
				_pos = 0;
			}
		}

		T &get(int i){
			return _values[i];
		}
	};

	template<class T>
	class ring<T>::iterator{
	private:
		int _pos;
		ring _ring;
	public:
		iterator(int pos, const ring &buffer) : _pos(pos), _ring(buffer){

		}

		//postfix operator
		iterator operator++(int){
			iterator it = *this;
			++_pos;
			return it;
		}

		//prefix operator
		iterator &operator++(){
				++_pos;
				return *this;
		}

		T &operator*(){
			return _ring.get(_pos);
		}

		bool operator!=(const iterator &rhs) const{
			return _pos != rhs._pos;
		}
	};

} /* namespace udemy */

#endif /* RING_H_ */
