/*
 * Complex.cpp
 *
 *  Created on: 23 maj 2020
 *      Author: rogerisaksson
 */
#include <iostream>
#include "Complex.h"

namespace udemy {

	Complex::Complex() :_real(0), _imaginary(0) {}

	Complex::Complex(double real, double imaginary) : _real(real), _imaginary(imaginary){}

	Complex::Complex(const Complex &copy){
		*this = copy;
	}

	Complex::~Complex(){

	}

	bool Complex::operator<(const Complex &rhs){
		return (_real == rhs._real) && (_imaginary == rhs._imaginary);
	}

	bool Complex::operator==(const Complex &rhs) const{
		return _real == rhs._real && _imaginary == rhs._imaginary;
	}

	bool Complex::operator!=(const Complex &a) const{
		return !(*this==a);
	}

	bool Complex::operator==(double a) const{
		return _real == a;
	}

	bool Complex::operator!=(double a) const{
		return !(*this==a);
	}

	const Complex &Complex::operator=(const Complex &in){
		_real = in._real;
		_imaginary = in._imaginary;
		return *this;
	}

	std::ostream & operator<<(std::ostream &lhs, const Complex &rhs){
		lhs << rhs._real << ' ' << rhs._imaginary << 'i' << std::flush;
		return lhs;
	}

	const Complex Complex::operator+(const Complex &rhs) const{
		return Complex(this->_real + rhs._real, this->_imaginary + rhs._imaginary);
	}

	Complex operator+(const Complex &lhs, const double rhs){
		return Complex(lhs._real + rhs, lhs._imaginary);
	}

	Complex operator+(const double lhs, const Complex &rhs){
		return Complex(rhs._real + lhs, rhs._imaginary);
	}

	// Complex Conjugate
	const Complex Complex::operator*() const{
		return Complex(_real, -_imaginary);
	}

	const Complex Complex::operator*(const Complex &rhs) const{
		return Complex(this->_real * rhs._real +
						(this->_imaginary * rhs._imaginary) * -1.0,
						this->_real * rhs._imaginary +
						this->_imaginary * rhs._real);
	}

	Complex operator*(const double lhs, const Complex &rhs){
		return Complex(rhs._real*lhs, rhs._imaginary*lhs);
	}

	Complex operator*(const Complex &lhs, const double rhs){
			return Complex(lhs._real*rhs, lhs._imaginary*rhs);
		}

	Complex operator+=(const Complex &rhs, const Complex &lhs){
		return Complex(rhs._real + lhs._real, rhs._imaginary + lhs._imaginary);
	}

	Complex operator+=(const Complex &a, double real){
		return Complex(a._real + real, a._imaginary);
	}

	// Define prefix increment operator.
	Complex &Complex::operator++(){
		_real = _real + 1.0;
		return *this;
	}

	// Define postfix increment operator.
	Complex Complex::operator++(int){
		Complex temp = *this;
		++*this; //call prefix increment operator
		return temp;
	}

} /* namespace udemy */
