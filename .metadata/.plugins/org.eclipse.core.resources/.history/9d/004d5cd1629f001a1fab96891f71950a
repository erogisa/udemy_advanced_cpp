#include <iostream>
#include <vector>

class Test{
	static const int SIZE = 100;
	int _items = 0;
	int *_pbuffer = nullptr;

public:

	Test(){
		std::cout << "Constructor" << std::endl;
		_pbuffer = new int[SIZE]{};
	}

	Test(int items) : Test(){
		_items = items;
		std::cout << "Parametrized Constructor" << std::endl;
		for(int i = 0; i < _items; ++i){
			_pbuffer[i] = 10+i;
		}
	}

	~Test(){
		std::cout << "Destructor" << std::endl;
		if(_pbuffer){
			delete[] _pbuffer;
		}
	}

	//Copy constructor
	Test(const Test &rhs)
	{
		std::cout << "Copy Constructor" << std::endl;
		copy(rhs);
	}

	void copy(const Test &copy){
		_items = copy._items;
		if(!_pbuffer){
			_pbuffer = new int[SIZE]();
		}
		std::memcpy(_pbuffer, copy._pbuffer, SIZE*sizeof(int));
	}

	//Assignment operator
	Test &operator=(const Test &rhs){
		std::cout << "Assignment Operator" << std::endl;
		copy(rhs);
		return *this;
	}

	//Move operator
	Test& operator=(Test &&rhs){
		std::cout << "Move Operator" << std::endl;
		if(this != &rhs) {
			if(_pbuffer){
				delete[] _pbuffer;   //Delete the string's original data
				_pbuffer = nullptr;
			}
			rhs._items = 0;			 // Reset number of items in the buffer
			_pbuffer = rhs._pbuffer; // Swap pointers to the allocated memory
			rhs._pbuffer = nullptr;  // Finally set to nullptr
		}
	    return *this;
	}

	friend std::ostream &operator<<(std::ostream &lhs, const Test &rhs){
		for(int i = 0; i < rhs._items; ++i){
			lhs << "_pbuffer items: " << rhs._pbuffer[i] << std::endl;
		}
		return lhs;
	}
};

Test getTest(){
	return Test();
}

class Child : public Test{
public:
	Child(){
	}
};

int main(int argc, char **argv) {

	Test test2(10);				//calls constructor and parametrized constructor
	Test test1 = getTest();		//calls constructor
	Test test3 = test2; 		//calls copy constructor
	test1 = test2;				//calls assignment operator
	Test *testptr1 = &test1;
	*testptr1 = test1; 			//calls assignment operator
	test2 = std::move(test1); 	//calls move operator test1 -> test2 then deleting test2 data

	std::cout << test1 << std::endl;
	std::cout << test2 << std::endl; //test2 empty
	std::cout << test3 << std::endl;

	const Test &lref = getTest(); //lvalue temporary binds to const
	std::cout << lref << std::endl;

	Test test4(Test(5));
	std::cout << test4 << std::endl;

	Test &&rRef(getTest());
	std::cout << rRef << std::endl;

	std::cout << "Hello world" << std::endl;
	return 0;
}
