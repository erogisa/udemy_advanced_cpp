/*
 * fractal_creator.h
 *
 *  Created on: 28 maj 2020
 *      Author: rogerisaksson
 */

#ifndef FRACTAL_CREATOR_H_
#define FRACTAL_CREATOR_H_

#include <iostream>
#include <memory>
#include <vector>
#include "zoom.h"
#include "zoomlist.h"
#include "mandelbrot.h"
#include "bitmap.h"
#include "rgbgenerator.h"

namespace udemy {

class fractal_creator {
private:
	int _width;
	int _height;
	std::unique_ptr<int[]> _histogram{nullptr};
	std::unique_ptr<int[]> _fractal{nullptr};
	udemy::bitmap _bitmap;
	udemy::zoomlist _zoomlist;
	int _total_iterations=0;
	std::vector<double> _ranges{0};
	std::vector<udemy::rgb_generator> _colors;


public:
	fractal_creator(int width, int height);
	void add_range(double end_range, const udemy::rgb_generator &rgb);
	void add_zoom(const udemy::zoom &zoom);
	void generate_fractal_image(std::string filename);

private:
	void calculate_iterations();
	void calculate_total_iterations();
	void draw_fractal();
	void write_bitmap(std::string);
};

} /* namespace udemy */

#endif /* FRACTAL_CREATOR_H_ */
