/*
 * fractal_creator.h
 *
 *  Created on: 28 maj 2020
 *      Author: rogerisaksson
 */

#ifndef FRACTAL_CREATOR_H_
#define FRACTAL_CREATOR_H_

#include <iostream>
#include <memory>
#include "zoom.h"
#include "zoomlist.h"
#include "mandelbrot.h"
#include "bitmap.h"

namespace udemy {

class fractal_creator {
private:
	int _width;
	int _height;
	std::unique_ptr<int[]> _histogram{nullptr};
	std::unique_ptr<int[]> _fractal{nullptr};
	udemy::bitmap _bitmap;
	udemy::zoomlist _zoomlist;
	int _total_iterations=0;

public:
	fractal_creator(int width, int height);
	void generate_fractal_image();
	void calculate_iterations();
	void calculate_total_iterations();
	void draw_fractal();
	void add_zoom(const udemy::zoom &zoom);
	void write_bitmap(std::string);
};

} /* namespace udemy */

#endif /* FRACTAL_CREATOR_H_ */
