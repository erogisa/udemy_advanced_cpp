#include <iostream>
#include <vector>

class Parent{
	int _dogs;
	std::string _text;
public:

	Parent() : Parent("Hello String Constructor"){
		_dogs=5;
		std::cout << "No Parameter Constructor" << std::endl;
	}

	Parent(std::string text){
		_dogs = 5;
		_text = "Kalle";
		std::cout << "String Parameter Constructor" << std::endl;
	}

	virtual ~Parent(){};

	virtual void print(){
		std::cout << "Hello Parent" << std::endl;
	}

};

class Child : public Parent{
public:
	Child() : Parent(){

	}

	Child(const Child &copy){
	}

	Child(Child &&copy){
	}

	virtual void print() override{
		std::cout << "Hello Child" << std::endl;
	}
};

int main(int argc, char **argv) {

	Parent parent("Hello");
	std::cout << std::endl;
	Child child;

	std::shared_ptr<Parent> ptr(new Child);

	ptr->print();

	int cats = 5;

	std::cout << "Hello world";
	return 0;
}
