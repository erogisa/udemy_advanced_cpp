#include <iostream>
#include <vector>
#include <initializer_list>

class Test{
public:
	Test();

};

int main(int argc, char **argv) {

	int integers[]{1, 2, 3, 4};

	for(auto i : integers){
		std::cout << i << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}
