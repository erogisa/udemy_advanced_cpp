/*
 * bitmap.h
 *
 *  Created on: 27 maj 2020
 *      Author: rogerisaksson
 */

#ifndef BITMAP_H_
#define BITMAP_H_

#include <cstdint>
#include <iostream>
#include <string>
#include <memory>


namespace udemy {

class bitmap {
private:
	std::int32_t _width{0};
	std::int32_t _height{0};
	std::unique_ptr<std::uint8_t[]> _pixels{nullptr};

public:
	bitmap();
	bitmap(std::int32_t width, std::int32_t height);
	virtual ~bitmap();
	bitmap(const bitmap &other);
	bitmap(bitmap &&other);
	bitmap& operator=(const bitmap &other);
	bitmap& operator=(bitmap &&other);

	bool write_file(std::string file_name);
	void set_pixel(int x, int y, std::uint8_t red, std::uint8_t green, std::uint8_t blue);
};

} /* namespace udemy */

#endif /* BITMAP_H_ */
