/*
 * bitmap.cpp
 *
 *  Created on: 27 maj 2020
 *      Author: rogerisaksson
 */

#include <fstream>
#include "bitmap.h"
#include "bitmap_file_header.h"
#include "bitmap_info_header.h"

namespace udemy {

bitmap::bitmap() {
	// TODO Auto-generated constructor stub

}

bitmap::bitmap(std::int32_t width, std::int32_t height) :
				_width(width), _height(height),
				_pixels(new std::uint8_t[width * height * 3]{
				[1 ... 10] = 254
}) {

}

bitmap::~bitmap() = default;


bitmap::bitmap(bitmap &&other) = default;

bool bitmap::write_file(std::string file_name) {
	udemy::bitmap_file_header file_header;
	udemy::bitmap_info_header file_info;

	file_header.file_size = sizeof(udemy::bitmap_file_header) +
							sizeof(udemy::bitmap_info_header) +
							(3 * _width * _height)*sizeof(std::uint8_t);
	file_header.data_offset = sizeof(udemy::bitmap_file_header) +
							sizeof(udemy::bitmap_info_header);
	file_info.width = _width;
	file_info.height = _height;

	std::fstream bitmap_file_stream;
	bitmap_file_stream.open(file_name, std::ios::out | std::ios::binary);

	if(!bitmap_file_stream.is_open()){
		return false;
	}

	bitmap_file_stream.write(reinterpret_cast<char*>(&file_header),
							sizeof(udemy::bitmap_file_header));

	bitmap_file_stream.write(reinterpret_cast<char*>(&file_info),
							sizeof(udemy::bitmap_info_header));

	bitmap_file_stream.write(reinterpret_cast<char*>(_pixels.get()),
							3*_width*_height*sizeof(std::uint8_t));

	bitmap_file_stream.flush();

	bitmap_file_stream.close();

	if(!bitmap_file_stream.is_open()){
		return false;
	}

	return true;
}

void bitmap::set_pixel(int x, int y,
						std::uint8_t red,
						std::uint8_t green,
						std::uint8_t blue) {

	std::uint8_t *pixel = _pixels.get()+(3*y*_width)+(3*x);
	pixel[0] = blue;
	pixel[1] = green;
	pixel[2] = red;

}

} /* namespace udemy */
