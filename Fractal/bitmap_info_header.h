/*
 * bitmap_file_header.h
 *
 *  Created on: 27 maj 2020
 *      Author: rogerisaksson
 */

#ifndef BITMAP_INFO_HEADER_H_
#define BITMAP_INFO_HEADER_H_

#include <cstdint>

namespace udemy {

#pragma pack(push, 2)

struct bitmap_info_header {
	std::int32_t headerSize { 40 };
	std::int32_t width;
	std::int32_t height;
	std::int16_t planes { 1 };
	std::int16_t bitsPerPixel { 24 };
	std::int32_t compression { 0 };
	std::int32_t dataSize { 0 };
	std::int32_t horizontalResolution { 2400 };
	std::int32_t verticalResolution { 2400 };
	std::int32_t colors { 0 };
	std::int32_t importantColors { 0 };
};

#pragma pack(pop)

}

#endif /* BITMAP_FILE_HEADER_H_ */
