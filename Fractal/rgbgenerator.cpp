/*
 * rgbgenerator.cpp
 *
 *  Created on: 28 maj 2020
 *      Author: rogerisaksson
 */

#include "rgbgenerator.h"

namespace udemy {

rgb_generator::rgb_generator(double r, double g, double b) :
	_r(r), _g(g), _b(b){
	// TODO Auto-generated constructor stub

}

rgb_generator operator-(const rgb_generator &lhs, const rgb_generator &rhs){
	return rgb_generator(lhs._r-rhs._r, lhs._g-rhs._g, lhs._b-rhs._b);

}


} /* namespace udemy */
