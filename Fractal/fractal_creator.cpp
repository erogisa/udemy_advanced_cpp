/*
 * fractalcreator.cpp
 *
 *  Created on: 28 maj 2020
 *      Author: rogerisaksson
 */

#include "fractal_creator.h"

namespace udemy {

fractal_creator::fractal_creator(int width, int height):
		_width(width),
		_height(height),
		_histogram(new int[udemy::mandelbrot::MAX_ITERATIONS]{}),
		_fractal(new int[_width* _height]{ 0 }),
		_bitmap(_width, _height),
		_zoomlist(_width, _height)
{
	_zoomlist.add(udemy::zoom(_width / 2, _height / 2, 4.0 / _width));

}

void fractal_creator::add_range(double end_range, const udemy::rgb_generator &rgb){
	_ranges.push_back(end_range*udemy::mandelbrot::MAX_ITERATIONS);
	_colors.push_back(rgb);
	if(_first_in_range){
		_ranges_total.push_back(0);
	}
	_first_in_range = true;
}

void fractal_creator::add_zoom(const udemy::zoom &zoom){
	_zoomlist.add(zoom);

}

void fractal_creator::generate_fractal_image(std::string filename){

	calculate_iterations();
	calculate_total_iterations();
	calculate_range_total();
	draw_fractal();
	write_bitmap(filename);
}

void fractal_creator::calculate_iterations(){
	for(int y = 0; y < _height; y++){
		for(int x = 0; x < _width; x++){

			std::pair<double, double> coords = _zoomlist.doZoom(x,y);
			int iterations = udemy::mandelbrot::getIterations(coords.first, coords.second);

			_fractal[y*_width + x]=iterations;

			if(iterations != udemy::mandelbrot::MAX_ITERATIONS) {

				_histogram[iterations]++;

			}
		}
	}
}

void fractal_creator::calculate_range_total(){

	int range_index = 0;

	for(int i = 0; i<udemy::mandelbrot::MAX_ITERATIONS; i++){
		int pixels = _histogram[i];

		if(i >=_ranges[range_index+1]){
			range_index++;
		}
		_ranges_total[range_index] += pixels;
	}

	for(auto total : _ranges_total){
		std::cout << total << std::endl;
	}

}


void fractal_creator::calculate_total_iterations(){
	for(int i = 0; i<udemy::mandelbrot::MAX_ITERATIONS; ++i){
		_total_iterations+=_histogram[i];
	}
}

void fractal_creator::draw_fractal(){

	for(int y = 0; y < _height; y++){
		for(int x = 0; x < _width; x++){

			int iterations = _fractal[y*_width + x];

			int range = get_range(iterations);
			int range_total = _ranges_total[range];
			int range_start = _ranges[range];

			udemy::rgb_generator &start_color = _colors[range];
			udemy::rgb_generator &end_color = _colors[range+1];

			udemy::rgb_generator diff = start_color - end_color;

			std::uint8_t red = 0;
			std::uint8_t green = 0;
			std::uint8_t blue = 0;


			if(iterations!=udemy::mandelbrot::MAX_ITERATIONS){

				double hue = 0.0;
				int total_pixels = 0;

				for(int i = range_start; i <= iterations; i++){
					total_pixels += _histogram[i];
				}

				red = start_color._r + diff._r*((double)total_pixels/(double)range_total);
				green = start_color._g + diff._g*((double)total_pixels/(double)range_total);
				blue = start_color._b + diff._b*((double)total_pixels/(double)range_total);

			}
			_bitmap.set_pixel(x, y, red, green, blue);
		}
	}

}

void fractal_creator::write_bitmap(std::string filename){
	_bitmap.write_file(filename);

}

int fractal_creator::get_range(int iterations) const{
	int range = 0;
	for(int i = 1; i <_ranges.size(); i++){
		range = i;
		if(_ranges[i] > iterations){
			break;
		}
	}
	range--;
	assert(range > -1);
	assert(range < _ranges.size());
	return range;
}

} /* namespace udemy */
