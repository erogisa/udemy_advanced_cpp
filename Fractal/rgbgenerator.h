/*
 * rgbgenerator.h
 *
 *  Created on: 28 maj 2020
 *      Author: rogerisaksson
 */

#ifndef RGBGENERATOR_H_
#define RGBGENERATOR_H_

#include <iostream>

namespace udemy {

struct rgb_generator {
	double _r = 0.0;
	double _g = 0.0;
	double _b = 0.0;
	rgb_generator(double r, double g, double b);
	friend rgb_generator operator-(const rgb_generator &lhs, const rgb_generator &rhs);

};



} /* namespace udemy */

#endif /* RGBGENERATOR_H_ */
