/*
 * zoomlist.h
 *
 *  Created on: 27 maj 2020
 *      Author: rogerisaksson
 */

#ifndef ZOOMLIST_H_
#define ZOOMLIST_H_

#include <iostream>
#include <vector>
#include <utility>
#include "zoom.h"

namespace udemy {

class zoomlist {
private:
	double _x_center = 0;
	double _y_center = 0;
	double _scale = 1.0;
	std::vector<udemy::zoom> _zooms;
	int _width = 0;
	int _height = 0;
public:
	zoomlist(int width, int height);
	void add(const udemy::zoom &toZoom);
	std::pair<double, double> doZoom(int x, int y);

};
} /* namespace udemy */

#endif /* ZOOMLIST_H_ */
