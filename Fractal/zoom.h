/*
 * zoom.h
 *
 *  Created on: 27 maj 2020
 *      Author: rogerisaksson
 */

#ifndef ZOOM_H_
#define ZOOM_H_

namespace udemy {

struct zoom {
public:
	int _x = 0;
	int _y = 0;
	double _scale = 0;

	zoom(int x, int y, double scale): _x(x), _y(y), _scale(scale){

	}



};

} /* namespace udemy */

#endif /* ZOOM_H_ */
