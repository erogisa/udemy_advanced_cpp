/*
 * bitmap_file_header.h
 *
 *  Created on: 27 maj 2020
 *      Author: rogerisaksson
 */

#ifndef BITMAP_FILE_HEADER_H_
#define BITMAP_FILE_HEADER_H_

#include <cstdint>

namespace udemy {

#pragma pack(push, 2)

struct bitmap_file_header {

	char header[2] { 'B', 'M' };
	std::int32_t file_size;
	std::int32_t reserved;
	std::int32_t data_offset;

};

#pragma pack(pop)

}

#endif /* BITMAP_FILE_HEADER_H_ */
