#include <iostream>
#include <vector>
#include <map>

class Test{
private:
	int _id;
	std::string _name;

public:
	Test() : _id(0), _name(""){}

	Test(int id, std::string name) : _id(id), _name(name){}

	Test(const Test &copy) : _id(copy._id), _name(copy._name){}

	~Test(){
		_id = 0;
		_name = "";
		std::cout << "Destructor hit" << std::endl;
	}

	bool operator<(const Test &compare) const{
		return _id < compare._id;
	}


	static bool compById(const Test* a, const Test* b)
	{
		return *a < *b;
	}

	friend std::ostream &operator<<(std::ostream &os, const Test &test){
		os << test._id << " " << test._name << std::flush;
		return os;
	}
};

int main(int argc, char **argv) {

	std::map<std::string, std::vector<Test*> > scores;

	scores["Robotics"].push_back(new Test(10, "Nisse"));
	scores["Maths"].push_back(new Test(20, "Olle"));
	scores["Maths"].push_back(new Test(45, "Kalle"));
	scores["Feedback Control"].push_back(new Test(10, "Pelle"));

	for(const auto entry : scores){
		std::cout << entry.first << std::endl;
		for(const auto score : entry.second){
			std::cout << *score << std::endl << std::flush;
		}
		std::cout << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}
