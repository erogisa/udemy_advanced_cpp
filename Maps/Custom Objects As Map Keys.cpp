#include <iostream>
#include <map>

class Person{
private:
	std::string _name;
	int _age;

public:

	friend std::ostream& operator<<(std::ostream& os, const Person& person);

	Person() : _name(""), _age(0){

	}

	Person(const Person &copy){
		_name = copy._name;
		_age = copy._age;
		//std::cout << "Copy constructor running: " << _name << " " << _age << std::endl;
	}

	Person(std::string name, int age) : _name(name), _age(age){

	}

	friend std::ostream& operator<<(std::ostream& os, const Person &person){
		os << "Name is: " << person._name << " Age is: " << person._age;
		return os;

	}

	bool operator< (const Person &compare) const{
		return _name < compare._name;
	}

};

int main(int argc, char **argv) {

	std::map<Person, int > people;

	people[Person("Kalle", 10)] = 0;
	people[Person("Pelle", 20)] = 1;
	people[Person("Olle", 30)] = 2;
	people.insert(std::make_pair(Person("Nisse", 45),40));
	people.insert(std::pair< Person, int>(Person("Evert", 33),20));

	for(auto &val : people){
		std::pair<Person, int> test = val;
		std::cout << val.second << " ";
		std::cout << val.first << std::endl;
	}

	if(people.find(Person("Olle", 30)) != people.end()){
		auto it = people.find(Person("Olle", 30));
	}
	else{
		std::cout << "Vicky is not in Map" << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}
