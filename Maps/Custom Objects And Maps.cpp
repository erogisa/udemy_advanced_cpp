#include <iostream>
#include <map>

class Person{
private:
	std::string _name;
	int _age;

public:

	Person() : _name(""), _age(0){

	}

	Person(const Person &copy){
		_name = copy._name;
		_age = copy._age;
		std::cout << "Copy constructor running: " << _name << " " << _age << std::endl;
	}

	Person(std::string name, int age) : _name(name), _age(age){

	}

	void print() const{
		std::cout << "Name is: " << _name << " Age is: " << _age << std::endl;
	}
};

int main(int argc, char **argv) {

	std::map< int, Person > people;

	people[0] = Person("Kalle", 10);
	people[1] = Person("Pelle", 20);
	people[2] = Person("Olle", 30);
	people.insert(std::make_pair(40, Person("Nisse", 45)));
	people.insert(std::pair<int, Person>(20, Person("Evert", 33)));

	people[0].print();

	for(const auto &val : people){
		const std::pair<int, Person> test = val;
		std::cout << val.first << " ";
		test.second.print();
	}

	if(people.find(0) != people.end()){
		people[0].print();
	}
	else{
		std::cout << "Vicky is not in Map" << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}
