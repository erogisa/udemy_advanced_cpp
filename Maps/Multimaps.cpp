#include <iostream>
#include <map>
#include <utility>

int main(int argc, char **argv) {

	std::multimap<int, std::string> lookup;

	lookup.insert(std::make_pair(30, "Nisse"));
	lookup.insert(std::make_pair(10, "Olle"));
	lookup.insert(std::make_pair(30, "Pelle"));
	lookup.insert(std::make_pair(20, "Svetsi"));

	for(auto const &val : lookup){
		std::cout << val.first << " " << val.second << std::endl;
	}

	std::cout << std::endl;

	std::pair<std::multimap<int, std::string>::iterator, std::multimap<int, std::string>::iterator> its = lookup.equal_range(30);

	for(auto &i = its.first; i!=its.second; i++){
			std::cout << i->first << " " << i->second << std::endl;
		}

	std::cout << std::endl;

	for(auto i = lookup.equal_range(20).first; i != lookup.equal_range(30).second; i++){
		std::cout << i->first << " " << i->second << std::endl;
	}

	std::cout << std::endl;

	return 0;
}
