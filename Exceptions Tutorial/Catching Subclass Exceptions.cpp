#include <iostream>
#include <exception>


void goesWrong(){
	bool error1Detected = false;
	bool error2Detected = true;

	if(error1Detected){
		throw std::bad_alloc();
	}

	if(error2Detected){
		throw std::exception();
	}
}

int main(int argc, char **argv) {

	try{
		goesWrong();
	}
	//catch in the order of inheritance, child class before
	catch(std::bad_alloc &ex){
		std::cout << "Catching bad_alloc: " << ex.what() << std::endl;
	}

	catch(std::exception &ex){
		std::cout << "Catching exception: "<< ex.what() << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}

