#include <iostream>



void mightGoWrong(){
	bool error1 = false;
	bool error2 = true;

	if(error1){
		throw "Something went wrong\n";
	}
	if(error2){
		throw std::string("Something else went wrong\n");
	}
}

void usesMightGoWrong(){
	mightGoWrong();
}

int main(int argc, char **argv) {
	try{
		usesMightGoWrong();
	}
	catch(int e){
		std::cout << "Error Code: " << e << std::endl;
	}
	catch(char const *str){
		std::cout << str;
	}
	catch(std::string &str){
		std::cout << str;
	}

	std::cout << "Hello world";
	return 0;
}

