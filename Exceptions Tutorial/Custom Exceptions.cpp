#include <iostream>
#include <exception>


class MyException: public std::exception{
public:
	virtual const char* what(){
		return "Something bad happened\n";
	}
};

class Test{
public:
	void GoesWrong(){
		throw MyException();
	}
};


int main(int argc, char **argv) {
	Test test;

	try{
		test.GoesWrong();
	}
	catch(MyException &ex)
	{
		std::cout << "Shit hit the fan: " << ex.what() << std::endl;
	}

	std::cout << "Hello world";
	return 0;
}

