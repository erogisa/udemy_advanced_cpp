#include <iostream>
#include <vector>

int main(int argc, char **argv) {

	std::vector<std::vector<int> > grid(3, std::vector<int>(4,7));

	for(const auto& row : grid)
	{
		for(const auto& col : row){
			std::cout << col << std::flush;
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;

	for(int i = 0; i<grid.size(); ++i){
		for(int j = 0; j<grid[i].size(); ++j){
			std::cout << grid[i][j] << std::flush;
		}
		std::cout << std::endl;
	}

	return 0;


}
