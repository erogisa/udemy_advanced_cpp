#include <iostream>
#include <vector>

int main(int argc, char **argv) {

	std::vector<double> numbers(20);

	std::cout << numbers.capacity() << std::endl;

	for(int i = 0; i < 10000; ++i){
		numbers.push_back((double) i);
		if(i % 1000 == 0){
			std::cout << numbers.capacity() << std::endl;
		}
	}

	numbers.clear();

	std::cout << numbers.capacity() << std::endl;

	return 0;

}
