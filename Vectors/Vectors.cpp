#include <iostream>
#include <vector>

int main(int argc, char **argv) {

	std::vector<std::string> vec;

	vec.push_back("zero");
	vec.push_back("one");
	vec.push_back("two");

	for(std::vector<const std::string>::iterator it = vec.begin(); it!=vec.end(); it++){
		std::cout << *it << std::endl;

	}

	return 0;

}
