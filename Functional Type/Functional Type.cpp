#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>

//function (pointer)
bool check(std::string s){
	return 5 == s.size();
}

//Callable
void run(std::function<bool(std::string&)> check){
	std::string str{"stars"};
	std::cout << check(str) << std::endl;
}

//Functor
class Check{
public:
	bool operator()(std::string &str) const{
		return 5 == str.size();
	}
}check1;

int main(int argc, char **argv) {

	std::vector<std::string> vec{"one", "two", "three"};

	int match = 5;

	//Lambda
	auto countLambda = std::count_if(vec.begin(), vec.end(), [match](std::string test)->bool{
											return test.size()==match;});
	std::cout << countLambda << std::endl;

	//Function pointer
	auto count_fptr = std::count_if(vec.begin(), vec.end(), check);
	std::cout << count_fptr << std::endl;

	//Functor
	auto count_functor = std::count_if(vec.begin(), vec.end(), check1);
	std::cout << count_functor << std::endl;

	//Callable with lambda
	auto lambda = [match](std::string test){return test.size()==match;};
	run(lambda);

	//Callable with functor
	run(check1);

	//Callable with function pointer
	run(check);

	//Function
	std::function<int(int, int)> add = [](int one, int two){return one + two;};
	std::cout << add(1, 2) << std::endl;

	std::cout << "Hello World!" << std::endl;

	return 0;
}
